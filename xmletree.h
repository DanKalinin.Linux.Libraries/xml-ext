//
// Created by root on 09.09.2021.
//

#ifndef LIBRARY_XML_EXT_XMLETREE_H
#define LIBRARY_XML_EXT_XMLETREE_H

#include "xmlemain.h"










G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(xmlDoc, xmlFreeDoc)
G_DEFINE_AUTO_CLEANUP_FREE_FUNC(xmlDocPtr, xmlFreeDoc, NULL)

G_END_DECLS










G_BEGIN_DECLS

G_DEFINE_AUTOPTR_CLEANUP_FUNC(xmlNode, xmlFreeNode)
G_DEFINE_AUTO_CLEANUP_FREE_FUNC(xmlNodePtr, xmlFreeNode, NULL)

G_END_DECLS










#endif //LIBRARY_XML_EXT_XMLETREE_H
