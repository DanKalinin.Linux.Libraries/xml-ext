//
// Created by root on 09.09.2021.
//

#ifndef LIBRARY_XML_EXT_XMLEERROR_H
#define LIBRARY_XML_EXT_XMLEERROR_H

#include "xmlemain.h"

G_BEGIN_DECLS

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC(xmlError, xmlResetError)

void xmle_set_error(GError **self, xmlErrorPtr error);

G_END_DECLS

#endif //LIBRARY_XML_EXT_XMLEERROR_H
