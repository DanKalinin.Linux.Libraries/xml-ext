//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_XML_EXT_XMLEINIT_H
#define LIBRARY_XML_EXT_XMLEINIT_H

#include "xmlemain.h"

G_BEGIN_DECLS

void xmle_init(void);

G_END_DECLS

#endif //LIBRARY_XML_EXT_XMLEINIT_H
