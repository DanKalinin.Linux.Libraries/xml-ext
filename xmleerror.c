//
// Created by root on 09.09.2021.
//

#include "xmleerror.h"

void xmle_set_error(GError **self, xmlErrorPtr error) {
    g_set_error_literal(self, error->domain, error->code, error->message);
}
