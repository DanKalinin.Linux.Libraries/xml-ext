//
// Created by root on 10.09.2021.
//

#ifndef LIBRARY_XML_EXT_XMLEMISC_H
#define LIBRARY_XML_EXT_XMLEMISC_H

#include "xmlemain.h"

G_BEGIN_DECLS

#define XMLE_MIME_TYPE "application/xml"

G_END_DECLS

#endif //LIBRARY_XML_EXT_XMLEMISC_H
