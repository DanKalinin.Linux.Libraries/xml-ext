//
// Created by root on 09.09.2021.
//

#ifndef LIBRARY_XML_EXT_XMLEPARSER_H
#define LIBRARY_XML_EXT_XMLEPARSER_H

#include "xmlemain.h"
#include "xmleerror.h"

G_BEGIN_DECLS

xmlDocPtr xmleParseMemory(gchar *buffer, gint size, GError **error);

G_END_DECLS

#endif //LIBRARY_XML_EXT_XMLEPARSER_H
