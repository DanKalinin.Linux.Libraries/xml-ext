//
// Created by root on 09.09.2021.
//

#include "xmleparser.h"

xmlDocPtr xmleParseMemory(gchar *buffer, gint size, GError **error) {
    xmlResetLastError();
    xmlDocPtr ret = xmlParseMemory(buffer, size);

    if (ret == NULL) {
        xmle_set_error(error, xmlGetLastError());
    }

    return ret;
}
